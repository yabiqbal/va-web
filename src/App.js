import './App.css';
// import { BrowserRouter, HashRouter } from 'react-router-dom'
import Routes from './router/routes'

function App() {
  return (
    <div className="App">
      {/* <HashRouter> */}
      <Routes />
      {/* </HashRouter> */}
    </div>
  );
}

export default App;
