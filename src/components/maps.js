import React, { Component } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4themes_animated from "@amcharts/amcharts4/themes/animated"; 
import Modals from '../components/modalSignup'
import '../App.css'

am4core.useTheme(am4themes_animated);

// const Register = () => {
    class Maps extends Component {
      constructor(props){
        super(props);
        this.state = {
          isOpen: false,
          location: ''
        };
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
      }
        
        
        openModal = (location) => this.setState({ isOpen: true, location:location });
        closeModal = () => this.setState({ isOpen: false, location:'' });
      
        componentDidMount() {
          // Create map instance
          var chart = am4core.create("chartdiv", am4maps.MapChart);

          // Set map definition
          chart.geodataSource.url = "https://www.amcharts.com/lib/4/geodata/json/indonesiaHigh.json";
          chart.geodataSource.events.on("parseended", function(event) {
            let data = [];
            for(var i = 0; i < event.target.data.features.length; i++) {
              data.push({
                id: event.target.data.features[i].id,
                value: Math.round( Math.random() * 10000 )
              })
            }
            polygonSeries.data = data;
          })
      
          // Set projection
          chart.projection = new am4maps.projections.Mercator();

          // Add zoom control
          chart.zoomControl = new am4maps.ZoomControl();
      
        //   // Set initial zoom
          chart.homeZoomLevel = 1;
      
          // Create map polygon series
        //   var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
        //   polygonSeries.mapPolygons.template.strokeWidth = 0.5;
      
          let polygonSeries = new am4maps.MapPolygonSeries();
          polygonSeries.useGeodata = true;
          chart.series.push(polygonSeries);
      
          // Make map load polygon (like country names) data from GeoJSON
          polygonSeries.useGeodata = true;

        // let polygonTemplate = polygonSeries.mapPolygons.template;
        // polygonTemplate.tooltipText = "{name}: {id}";
        // polygonTemplate.fill = am4core.color("#96BDC6");
        

          /** Start -- Bagian Maluku */
          var series1 = chart.series.push(new am4maps.MapPolygonSeries());
          series1.name = "Filled";
          series1.useGeodata = true;
          series1.include = ["ID-MA", "ID-MU"];
          series1.mapPolygons.template.tooltipText = "{name} - Maluku";
          series1.mapPolygons.template.fill = am4core.color("#96BDC6");
          series1.fill = am4core.color("#96BDC6");
          series1.events.on("hit", function(ev) {
            this.openModal('Maluku')
          }, this);
          /** End -- Bagian Maluku */

          /** Start -- Bagian Sulawesi */
          var series2 = chart.series.push(new am4maps.MapPolygonSeries());
          series2.name = "Filled";
          series2.useGeodata = true;
          series2.include = ["ID-SA"];
          series2.mapPolygons.template.tooltipText = "{name}";
          series2.mapPolygons.template.fill = am4core.color("#96BDC6");
          series2.fill = am4core.color("#96BDC6");
          series2.events.on("hit", function(ev) {
            console.log(series2.mapPolygons.template.tooltipText)
            this.openModal('Sulawesi Utara')
          }, this);
          /** End -- Bagian Sulawesi */

          let homeButton = new am4core.Button();

          // Home Button 
          homeButton.events.on("hit", function() {
            chart.goHome();
          });

          homeButton.icon = new am4core.Sprite();
          homeButton.padding(7, 5, 7, 5);
          homeButton.width = 30;
          homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
          homeButton.marginBottom = 10;
          homeButton.parent = chart.zoomControl;
          homeButton.insertBefore(chart.zoomControl.plusButton);
            
        
        }

        
        render() {
      
        return (
        <div>
            <div id="chartdiv" style={{ width: "100%", height: "700px" }}>
                
            </div>
            <Modals show={this.state.isOpen} onHide={this.closeModal} location={this.state.location}/>

        </div>
    )
}}

export default Maps