import React from 'react' 
import { Modal, Form, Col, Container, Row} from 'react-bootstrap'
import facebook from './assets/img/facebook.png'
import google from './assets/img/google.png'
import twitter from './assets/img/twitter.png'

const ModalSignup = (props) => {
    return (
        <div>
            <Modal show={props.show}
            onHide = {props.onHide}
            aria-labelledby="contained-modal-title-vcenter"
            centered>
            <Modal.Header closeButton>
              <Container>
              <h2><b>Sign Up</b></h2>
              <p>It's quick and easy</p>
              </Container>
            </Modal.Header>
            
            <Modal.Body className="show-grid">
              <Container>
              <Form className="form-signup" >
                <Form.Group class="needs-validation" novalidate>
                  <Form.Control class="form-control" value={`Your location ${props.location}`} />
                  
                </Form.Group>
                <Form.Row>
                  <Form.Group as={Col} controlId="firstName" className="needs-validation" novalidate>
                    <Form.Control className="form-control was-validated" type="text" placeholder="First name" required/>
                  </Form.Group>

                  <Form.Group as={Col} controlId="username">
                    <Form.Control type="text" placeholder="Username" required/>
                  </Form.Group>
                </Form.Row>

                <Form.Group controlId="email">
                  <Form.Control placeholder="Email address" type="email" required/>
                </Form.Group>

                <Form.Group controlId="password">
                  <Form.Control placeholder="New password" type="password" required/>
                </Form.Group>
                <Form.Label>Date of birth</Form.Label>

                <Form.Row>
                  <Form.Group as={Col} controlId="formGridCity">
                    <Form.Control as="select" defaultValue="28">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                      <option>6</option>
                      <option>7</option>
                      <option>8</option>
                      <option>9</option>
                      <option>10</option>
                      <option>11</option>
                      <option>13</option>
                      <option>14</option>
                      <option>15</option>
                      <option>16</option>
                      <option>17</option>
                      <option>18</option>
                      <option>19</option>
                      <option>20</option>
                      <option>21</option>
                      <option>22</option>
                      <option>23</option>
                      <option>24</option>
                      <option>25</option>
                      <option>26</option>
                      <option>27</option>
                      <option>28</option>
                      <option>29</option>
                      <option>30</option>
                      <option>31</option>
                    </Form.Control>
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridState">
                    <Form.Control as="select" defaultValue="Feb">
                      <option>Jan</option>
                      <option>Feb</option>
                      <option>Mar</option>
                      <option>Apr</option>
                      <option>Mei</option>
                      <option>Jun</option>
                      <option>Jul</option>
                      <option>Aug</option>
                      <option>Sept</option>
                      <option>Oct</option>
                      <option>Nov</option>
                      <option>Dec</option>
                    </Form.Control>
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridZip">
                    <Form.Control as="select" defaultValue="2021">
                      <option>2011</option>
                      <option>2012</option>
                      <option>2013</option>
                      <option>2014</option>
                      <option>2015</option>
                      <option>2016</option>
                      <option>2017</option>
                      <option>2018</option>
                      <option>2019</option>
                      <option>2020</option>
                      <option>...</option>
                    </Form.Control>
                  </Form.Group>
                </Form.Row>
                <Form.Label>Gender</Form.Label><br />
              {/* <Form.Group> */}
                <div class="wrapper">
                <input type="radio" name="select" id="option-1" checked />
                <input type="radio" name="select" id="option-2" />
                <label for="option-1" class="option option-1">
                  <div class="dot"></div>
                  <span>Female</span>
                </label>
                <label for="option-2" class="option option-2">
                  <div class="dot"></div>
                  <span>Male</span>
                </label>
              </div><br />
              {/* </Form.Group> */}
              <p className="term">
              By clicking Sign up, you agree to out <a href="Register">Terms, Data Policy and Cookie Policy. </a> 
              You may receive email notifications from us and can opt out at anytime.
              </p>
                <Row>
                  <Col lg={4} className="col-signup">
                  <button className="button-signup" type="submit" href="/"><b>Sign Up</b></button>
                 
                  </Col>
                  <Col>
                    <p className="or"> Or</p>
                  </Col>
                  
                  <Col sm={6}>
                    <a href="https://www.facebook.com" ><img src={facebook} className="social-icon" alt="facebook"/></a>
                    <a href="https://www.google.com" ><img src={google} className="social-icon" alt="google"/></a>
                    <a href="https://www.twitter.com" ><img src={twitter} className="social-icon" alt="twitter"/></a>
                  </Col>
                </Row>
              </Form>
              </Container>
            </Modal.Body>
          </Modal>
        </div>
    )
}

export default ModalSignup
