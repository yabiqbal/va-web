import React from 'react' 
import { Col, Row } from 'react-bootstrap'
import classified from '../assets/img/classified.png'
import logo from '../assets/img/name_logo.png'
import { FaHome } from 'react-icons/fa';
import { TiGroup } from 'react-icons/ti';
import { MdNotifications } from 'react-icons/md';
import { BiMessageDetail } from 'react-icons/bi';
import { Link } from 'react-router-dom'


const NavBars = () => {
    return (
        <div>
            <div className="navHeader">
            <Row>
                <Col md={6}>
                <Link to="/"><img src={logo} className="logo" width="20%" alt="logo"/>
                </Link>
                </Col>
                <Col>
                    <div className="containerNotif">
                    <Link to='/GeneralMember' className="navIcon"><h2><FaHome /> </h2> 
                    </Link>
                    </div>
                </Col>
                <Col>
                    <div className="containerNotif">
                    <Link to='/CulturalBase' className="navIcon"><h2><TiGroup /> </h2> 
                    <p >9</p>
                    </Link>
                    </div>
                </Col>
                <Col>
                    <div className="containerNotif">
                        {/* <a onClick={() => setModalShow(true)}>
                        <img src={chat} />
                        <p>9</p>
                        </a> */}
                    <Link to='/MainMCC' className="navIcon"><h2><BiMessageDetail /> </h2> 
                    <p >9</p>
                    </Link>
                    </div>
                </Col>
                <Col>
                    <div className="containerNotif1">
                    <Link to='/'><img src={classified} alt="classified"/>
                        <p className="classifiedIcon1">5</p>
                        </Link>
                    </div>
                </Col>
                <Col>
                    <div className="containerNotif">
                    <Link to='/MainMCC' className="navIcon" style={{color: "#930000"}}><h2><MdNotifications /> </h2> 
                    <p >9</p>
                    </Link>
                    </div>
                </Col>
            </Row>
            <br/>
            </div>
        </div>
    )
}

export default NavBars