import React, {useState} from 'react' 
import { Col, Container, Row, Nav, Form, NavDropdown } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { AiFillLike } from 'react-icons/ai';
import { FaShareAlt } from 'react-icons/fa';
import { BiMenu } from 'react-icons/bi';
import { IoMdChatboxes } from 'react-icons/io';
import { RiStarFill } from 'react-icons/ri';
import car from './assets/img/car.png'
import andre from './assets/img/andre.png'
import chandra from './assets/img/chandra.png'
import enteng from './assets/img/enteng.png'
import richard from './assets/img/richard.png'
import deviana from './assets/img/deviana.png'
import andy from './assets/img/andy.jpg'
import iphone from './assets/img/iphone.png'
import { FcIdea } from 'react-icons/fc';
import Chats from './chat';

const Profile2 = () => {
    const [modalShow, setModalShow] = useState(false);
    const [isShow, setIsShow] = useState(false);
    const handleShow = (condition) => setIsShow(condition);
    const handleClose = () => setModalShow(false)
    const [parseName, setParseName] = useState('');
    const person = [{'name': 'Andre', 'image': andre },{'name': 'Chandra', 'image': chandra}, {'name': 'Enteng', 'image': enteng}, {'name': 'Richard', 'image': richard},{'name': 'deviana', 'image': deviana}]
    
    const handlePerson = name =>  e  => {
        e.preventDefault();
        setParseName(name)
        handleShow(true);
        setModalShow(true)
      };
    return (
        <div>
            <Chats/>
            {/* <p>this is profile cultural</p> */}
            <Container>
            <div className="profile1">
            <div className="underForm">
            <Nav defaultActiveKey="/Snippets" as="ul">
            <Nav.Item as="li">
                <Nav.Link href="/Snippets">Snippets</Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
                <Nav.Link href="/home">Pals <i>3250</i></Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
                <Nav.Link href="/home">Image</Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
                <Nav.Link href="/home">Classified</Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
                <Nav.Link href="/home">Tales</Nav.Link>
            </Nav.Item>
            <NavDropdown title="More" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            </NavDropdown>
            </Nav>
            <img src={andy} className="imgform" alt="andy"/>
            <Form.Control placeholder="Let us know your thoughts" as="textarea" rows={3}/>
            <Nav defaultActiveKey="/home" as="ul">
            <Nav.Item className="underForm" as="li">
                <Nav.Link href="/home">Pals Request
                <p>9</p>
                </Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
                <Nav.Link eventKey="link-1">Billboard  
                <p>9</p>
                </Nav.Link>
            </Nav.Item>
            {/* <NavDropdown id="basic-nav-dropdown">

                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown> */}
            <div class="dropdown1">
            {/* <button class="dropbtn">Dropdown</button> */}
            <a href="/#/MainMCC" class="dropbtn"><h2><BiMenu /> </h2></a>

            <div class="dropdown-content">
            <a href="/#/MainMCC">Mom</a>
            <a href="/#/MainMCC">Dad</a>
            <a href="/#/MainMCC">Siblings</a>
            <a href="/#/MainMCC">Cousins</a>
            <a href="/#/MainMCC">Best Friends</a>
            <a href="/#/MainMCC">Friends at Work</a>
            <a href="/#/MainMCC">Business Friends</a>
            <a href="/#/MainMCC">General Friends</a>

            </div>
            </div>
            </Nav>
            </div>
            <div className="grup">
            <Container>
                <Row>
                <Col>
                        <p onClick={() => setModalShow(true)}><img src={andre} alt="andre"/>
                        <p>Andre</p>
                        <div className="star">
                        <h2><RiStarFill /> </h2>
                    </div>
                        </p>
                        </Col>
                        <Col>
                        <p ><img src={chandra} alt="chandra"/>
                        <p>Chandra</p>
                        </p>
                        </Col>
                        <Col>
                        <p ><img src={enteng} alt="enteng"/>
                        <p>Enteng</p>
                        </p>
                        </Col>
                        <Col>
                        {/* <img src={chandra}/> */}
                        </Col>
                </Row>
                <Row>
                        <Col >
                        <p ><img src={richard} alt="richard"/>
                        <p>Richard</p>
                        </p>
                        </Col>
                        <Col>
                        <p ><img src={deviana} alt="deviana"/>
                        <p>Deviana</p>
                        </p>
                        </Col>
                        <Col >
                        {/* <img src={chandra}/> */}
                        </Col>
                        <Col >
                        {/* <img src={chandra}/> */}
                        </Col>
                    </Row>
                </Container>
            </div> <br />
            <div className="car">
                <img src={car} alt="car"/>
            </div><br/>
            <div className="reviews">
            <br/>
                <Row>
                    <Col sm={1}>
                    <img src={andy} alt="andy"/>
                    </Col>
                    <Col sm={11}>
                    <p>This Morning i received a shocking news, the sudden passing of one of my very best friends
                        Dharma Sadini at 5.00 AM Saturday, in Los Angeles. Rest in Peace my brother... you are in
                        greatest hands now... The Lord will take care of your family. Miss you bro; miss singing 
                        and performing with you in our band the 4Q. poke Taufan Gsf.
                    </p>
                <hr/>
                    <Row>
                    <Col>
                            <Link to="/MainMCC" style={{color: '#930000'}}><h2><AiFillLike /> </h2>
                            </Link>
                            </Col>
                            <Col>
                            <Link to="/MusicCulture" style={{color: '#930000'}}><h2><IoMdChatboxes /> </h2>
                            </Link>
                            </Col>
                            <Col>
                            <Link to="/MainMCC" style={{color: '#930000'}}><h2><FaShareAlt /> </h2>
                            </Link>
                            </Col>
                    </Row>
                <hr/>
                    </Col>
                </Row>
                <Row>
                    <Col sm={1}>
                    <h3>MCC</h3>
                    </Col>
                    <Col sm={11}>
                    {/* <div className="grup"> */}
                
               <iframe width="90%" height="345" src="https://www.youtube.com/embed/RgKAFK5djSk?controls=0" title="youtube"><br/>
               </iframe>

            {/* </div>  */}
                <Row>
                        <Col >
                        <Link to="/MainMCC" style={{color: '#930000'}}><h2><AiFillLike /> </h2>
                        </Link>
                        </Col>
                        <Col>
                        <Link to="/MusicCulture" style={{color: '#930000'}}><h2><IoMdChatboxes /> </h2>
                            </Link> </Col>
                            <Col>
                            <Link to="/MainMCC" style={{color: '#930000'}}><h2><FcIdea /> </h2>
                            </Link>
                        </Col>
                        <Col>
                            <Link to="/MainMCC" style={{color: '#930000'}}><h2><FcIdea /> </h2>
                            </Link>
                        </Col>
                        
                    </Row>
                    <div className="iphone">
                     <img src={iphone} alt="iphone"/>
                    </div><br/>
                    </Col>
                    <hr />
                </Row>
                <Row>
                    
                    <Col sm={1}>
                    <img src={andy} alt="andy"/>
                    </Col>
                    
                    <Col>
                    <p>This Morning i received a shocking news, the sudden passing of one of my very best friends
                        Dharma Sadini at 5.00 AM Saturday, in Los Angeles. Rest in Peace my brother... you are in
                        greatest hands now... The Lord will take care of your family. Miss you bro; miss singing 
                        and performing with you in our band the 4Q. poke Taufan Gsf.
                    </p>
                <hr/>
                    <Row>
                    <Col>
                            <Link to="/MainMCC" style={{color: '#930000'}}><h2><AiFillLike /> </h2>
                            </Link>
                            </Col>
                            <Col>
                            <Link to="/MusicCulture" style={{color: '#930000'}}><h2><IoMdChatboxes /> </h2>
                            </Link>
                            </Col>
                            <Col>
                            <Link to="/MainMCC" style={{color: '#930000'}}><h2><FaShareAlt /> </h2>
                            </Link>
                            </Col>
                    </Row>
                <hr/>
                    </Col>
                </Row>
                
            </div><br />
            </div>
            <Chats show={modalShow} onHide={() => setModalShow(false)} />
            
            </Container>
        </div>
    )
}

export default Profile2