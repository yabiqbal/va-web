import React from 'react' 
import { Row, Col, Form, Carousel, Container } from 'react-bootstrap'
import '../App.css'
import Bali from '../components/assets/img/bali.png'
import Carousels from '../components/assets/img/carousel.png'
import logos from '../components/assets/img/name_logo.png'
import { Link } from 'react-router-dom'

const Login = () => {
    return (
        <div>
            <Row>
            <Col className="sidebar" lg={3}>
                <div>
                <br />
                <br />
                <Container>
                <Form>
                <Form.Group controlId="formBasicEmail">
                    <h4>Form or email</h4>
                    <Form.Control type="text" placeholder="" />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <h4>Form or email</h4>
                    <Form.Control type="text" placeholder="" />
                </Form.Group>
                </Form>
                </Container>
                <br />
                <br />
                <br />
                <img src={Bali} content="width=device-width, initial-scale=1.0" alt="bali"/>

                </div>
                </Col>
                <Col >
                <Carousel>
                <Carousel.Item interval={1000}>
                    <img
                    className="d-block w-100"
                    src={Carousels}
                    alt="First slide"
                    />
                </Carousel.Item>
                <Carousel.Item interval={500}>
                    <img
                    className="d-block w-100"
                    src={Carousels}
                    alt="Second slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={Carousels}
                    alt="Third slide"
                    />
                </Carousel.Item>
                </Carousel>
                </Col>
            </Row>
            <div className="footer-login">
                <Row>
                    <Col lg={4}>
                    <Link to='/'><img src={logos} className="logo" width="50%" alt="logos"/> 
                    </Link>
                    </Col>
                    <Col>
                    <Link to='/GeneralMember'>
                    <button  className="button-login" type="submit">Login</button>
                    </Link>
                    </Col>
                    <Col >
                    <Link to='/Register'>
                    <button className="button-creatacc" type="submit">Create Account</button>
                    </Link>
                    </Col>
                    <Col sm={3}>
                    <h4>Total Member: <i>500</i>k</h4>
                    </Col>
                </Row>
            </div>
        </div>
    )
}

export default Login