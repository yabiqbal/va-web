import React, {useState} from 'react'
import Modals from '../components/modalSignup'
import Maps from '../components/maps'
import Bali from '../components/assets/img/bali.png'
import logo from '../components/assets/img/n.png'
import logos from '../components/assets/img/name_logo.png'
import { Link } from 'react-router-dom'

import '../App.css'

import { Col, Row} from 'react-bootstrap'

const Register = () => {
    const [modalShow, setModalShow] = useState(false);
    return (
        <div>
            <Row>
                <Col className="sidebar" sm={3}>
                <div>
                    <img src={logo} alt="logo"/>

                </div>
                <img src={Bali} alt="bali"/>
                
                </Col>
                <Col sm={9}>
                    {/* <a onClick={() => setModalShow(true)}> */}
                    
                    <Maps/>

                    {/* </a> */}
                    <Modals show={modalShow} onHide={() => setModalShow(false)} />
                 
                </Col>
            </Row>
            <div className="footer-login">
                <Row>
                    <Col md={4}>
                    <Link to='/'><img src={logos} className="logo" width="50%" alt="logos"/> 
                    </Link>
                    </Col>
                </Row>
            </div>

        </div>
    )
}

export default Register