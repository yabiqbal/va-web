import {  Route, Switch } from 'react-router-dom'
import Login from '../page/login'
import Register from '../page/register'
import GeneralMember from '../page/generalMember'
import CulturalBase from '../page/culturalBase'
import MusicCulture from '../page/musicCulture'
import MainMCC from '../page/mainMCC'

const Routes = () => {
    return (
        // <HashRouter>
        <Switch>
            <Route exact path='/' component={Login} />
            <Route  path='/Login' component={Login} />
            <Route path='/Register' component={Register} />
            <Route  path='/GeneralMember' component={GeneralMember} />
            <Route  path='/CulturalBase' component={CulturalBase} />
            <Route  path='/MusicCulture' component={MusicCulture} />
            <Route  path='/MainMCC' component={MainMCC} />
            <Route render={function() {
                return (
                    <div>
                        <h1>404 Not Found</h1>
                    </div>
                )
            }} />
        </Switch>
        // </HashRouter>
    )
}

export default Routes